const express = require('express');
const mysql = require('mysql');

// create connection
const db = mysql.createConnection({
  host :'localhost',
  user : 'root',
  password: '',
  database : 'nodemysql'
})

//connect
db.connect((err) =>{
  if(err){
    // console.log('error');
    if(err) throw err;
  }
  console.log('Mysql connected...');
});

const app = express();

// Insert post 1
app.get('/addpost9',(req, res) =>{
  let post ={title:'Post Nine', body:'This is post number nine'};
  let sql = "INSERT INTO posts SET ?";
  let query = db.query(sql, post, (err, result) => {
    if(err) throw err;
    console.log(result);
    res.send('Posts 9 added...');
  });
});

// Select Posts
app.get('/getposts', (req, res) => {
  let sql = 'Select * from posts';
  let query  = db.query(sql , (err, results)=>{
      console.log(results);
      res.send('Posts fetched...');
  });
});

// Select individual post
app.get('/getpost/:id', (req, res) => {
  let sql = `Select * from posts where id = ${req.params.id}`;
  let query  = db.query(sql , (err, results)=>{
      console.log(results);
      res.send('Posts fetched...');
  });
});

// Select update post
app.get('/updatepost/:id', (req, res) => {
  let newTitle = 'Updated Title'
  let sql = `update posts set title = '${newTitle}' where id = ${req.params.id}`;
  let query  = db.query(sql , (err, results)=>{
      if(err) throw err;
      console.log(results);
      res.send('<i>Post updated...</i>');
  });
});

// Delete post
app.get('/deletepost/:id', (req, res) =>{
  let sql = `delete from posts where id = ${req.params.id}` ;
  let query = db.query(sql, (err, result)=>{
    if(err) throw err;
    console.log(result);
    res.send(`<i> Post <strong>${req.params.id}</strong> deleted.... </i>`)
  });
});

app.listen('3000', () => {
  console.log('Server started on port 3000');
});
